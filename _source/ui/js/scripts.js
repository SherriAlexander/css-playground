/* New modernizr test for all touch devices */
Modernizr.addTest('touchcapable', function () {
    var bool;
    if (
        ('ontouchstart' in window) || 
        (window.DocumentTouch && document instanceof DocumentTouch) || 
        (navigator.maxTouchPoints > 0) || 
        (navigator.msMaxTouchPoints > 0)
    ){
        // Secondary test to rule out some false positives
        if (
            (window.screen.width > 1279 && window.devicePixelRatio == 1) ||
            (window.screen.width > 1000 && window.innerWidth < (window.screen.width * 0.9)) // this checks if a user is using a resized browser window, not common on mobile devices
        ){
            bool = false;
        } else {
            bool = true;
        }
    } else {
        bool = false;
    }
    return bool;
});

/* New modernizr test to see if padding is included in min-height */
Modernizr.addTest('paddingaddedtominheight', function () {
    // create an element, set box-sizing to
    // border-box as well as absolutely position
    // the element off the screen.
    var div = document.createElement("div");
    div.style.MozBoxSizing = "border-box";
    div.style.OBoxSizing = "border-box";
    div.style.boxSizing = "border-box";
    div.style.minHeight = "100px";
    div.style.paddingTop = "10px";
    div.style.position = "absolute";
    div.style.top = "-2000px";

    // An element will not have height until it
    // is attached to the DOM.
    document.body.appendChild(div);

    // https://developer.mozilla.org/en/DOM/element.scrollHeight
    // scrollHeight is the height of the element
    // including its padding but not it's margin.
    var divHeight = div.scrollHeight;

    // clean up after ourselves.
    document.body.removeChild(div);
    return divHeight === 110;
});

/* Global namespace */
var TheLab = TheLab || {};

TheLab.data = TheLab.data || {};

TheLab.el = {
    html            : jQuery('html'),
    win             : jQuery(window),
    doc             : jQuery(document),
    body            : jQuery('body'),
    footer          : jQuery('.footer')
};

TheLab.isPageloaded = false;

TheLab.util = {
    hasTouch        : Modernizr.touchcapable,
    orientation     : function() {
        var _this = this;
        if (typeof orientation != 'undefined') {
            return (Math.abs(window.orientation) === 90)? "landscape" : "portrait";
        } else {
            return (_this.viewportWidth() >= _this.viewportHeight()) ? "landscape" : "portrait";
        }
    },
    isOldIE: function() {
        return ((jQuery.browser.msie) && (jQuery.browser.version <= 8))? true : false;
    },
    isOldSafari: function() {
        var userAgent = navigator.userAgent.toLowerCase(),
            browser   = '',
            version   = 0;
        userAgent = userAgent.substring(userAgent.indexOf('version/') + 8);
        userAgent = userAgent.substring(0, userAgent.indexOf('.'));
        version = userAgent;
        return ((jQuery.browser.safari) && (version < 7))? true : false;
    },
    forceRepaint: function(els, displaytype) {
        var $nodesToRepaint = jQuery(els);
        $nodesToRepaint.css('display','none');
        var temp = $nodesToRepaint.outerHeight();
        $nodesToRepaint.css('display', displaytype);
        //console.log("repaint finished");
    }
};


/* Set up link behaviors
   ========================================================================== */
TheLab.links = {
    init: function() {
        // Prevent jumping to top upon clicking blank links
        jQuery('a[href="#"]').click(function(e){e.preventDefault();});
        
        // Open PDF links in a new window
        jQuery('a[href$=".pdf"]').attr('target', '_blank');
    }
};


/* Set up IE9 responsive table fix
   ========================================================================== */
TheLab.responsiveTables = {
    init: function() {
        var $tables = jQuery('.table');
          
        if (TheLab.el.html.hasClass("lt-ie10")) {
            $tables.wrap("<div class='responsive-table'><div></div></div>");
        }
    }
};


/* Form functionality 
   ========================================================================== */
TheLab.forms = {
    formsToValidate: jQuery(".js-validate"),
    init: function() {
        var _this = this;

        _this.formsToValidate.each(function() {
            jQuery(this).parsley({
                errorClass: 'error',
                successClass: 'success',
                errorsWrapper: '<ul class="error"></ul>'
            });
        });
    }
};

/*
<h1 id="lb-ghost-title">
    <span class="lb-title-main"><span>L</span><span>i</span><span>v</span><span>i</span><span>n</span><span>g</span> <span>B</span><span>r</span><span>i</span><span>d</span><span>g</span><span>e</span><span>s</span></span>
    <span class="lb-title-ghost no-print" aria-hidden="true">
        <span>L</span><span>i</span><span>v</span><span>i</span><span>n</span><span>g</span> <span>B</span><span>r</span><span>i</span><span>d</span><span>g</span><span>e</span><span>s</span>
    </span>
    <span class="lb-title-ghost lb-ghost-top no-print" aria-hidden="true">
        <span>L</span><span>i</span><span>v</span><span>i</span><span>n</span><span>g</span> <span>B</span><span>r</span><span>i</span><span>d</span><span>g</span><span>e</span><span>s</span>
    </span>
</h1>
*/


/* Set up title ghosts
   ========================================================================== */
TheLab.ghostTitle = {
    title: jQuery("#lb-ghost-title"),
    titleText:"",
    init: function() {
        var _this = this;

        if (_this.title.length > 0) {
            _this.titleText = _this.title.text();
            console.log("Title text: " + _this.titleText);

            _this.addLetterSpans();
            _this.createGhosts();
        }
    },
    addLetterSpans: function() {
        var _this = this;

        var characters = _this.titleText.split("");
        var $masterSpan = jQuery('<span class="lb-title-main"></span>');

        jQuery.each(characters, function (i, el) {
            if (el !== " ") {
                $masterSpan.append('<span>' + el + '</span>');
            } else {
                $masterSpan.append(el);
            }
        });

        _this.title.empty();
        _this.title.append($masterSpan);
    },
    createGhosts: function() {
        var _this = this;

        var ghostContent = jQuery(".lb-title-main").html();
        var $ghostOne = jQuery('<span class="lb-title-ghost no-print" aria-hidden="true"></span>');
        var $ghostTwo = jQuery('<span class="lb-title-ghost lb-ghost-top no-print" aria-hidden="true"></span>');

        $ghostOne.append(ghostContent);
        $ghostTwo.append(ghostContent);

        this.title.append($ghostOne, $ghostTwo);
    }
};


/* Set up A11y toggletips (https://codepen.io/heydon/pen/Vzwdpy)
   ========================================================================== */
TheLab.toggletips = {
    toggletipTexts: jQuery("[data-toggletip]"),
    init: function() {
        var _this = this;

        if (_this.toggletipTexts.length > 0) {
            console.log("Initializing toggletips");

            _this.toggletipTexts.each(function(){
                var $this = jQuery(this);

                // Get message from original markup
                var message = $this.html();

                // Create the container element
                var $container = jQuery('<span class="toggletip-container"></span>');

                // Put it before the original element in the DOM
                $container.insertBefore($this);
                
                // Create the button element
                var $button = jQuery('<button type="button">More info <span aria-hidden="true">ⓘ</span></button>');
                $button.attr('data-toggletip-content', message);
                
                // Place the button element in the container
                $container.append($button);
                
                // Create the live region
                var $liveRegion = jQuery('<span role="status"></span>');
                
                // Place the live region in the container
                $container.append($liveRegion);
                
                // Remove the original element
                $this.remove();

                var instructions = "";
                
                // Toggle the message
                $button.on('click', function() {
                    $liveRegion.html('');
                    window.setTimeout(function() {
                        //if (jQuery("body").attr('data-whatinput') == "keyboard") {
                        //    instructions = "<p>Press ESC to close.</p>";
                        //} else {
                        //    instructions = "<p>Click anywhere else to close.</p>";
                        //}

                        $liveRegion.html('<span class="toggletip-bubble">'+ message + instructions +'</span>');
                    }, 100);
                });

                // Set up close on outside click
                jQuery(document).on("click.toggletip", function (e) {
                    if ($button !== jQuery(e.target)) {
                        $liveRegion.html('');
                    }                        
                });

                // Set up close on escape keypress
                jQuery(document).on("keydown.toggletip", function(e) {
                    var evt = e || window.event;
                    var isEscape = false;
                    if ("key" in evt) {
                        isEscape = (evt.key == "Escape" || evt.key == "Esc");
                    } else {
                        isEscape = (evt.keyCode == 27);
                    }
                    if (isEscape) {
                        $liveRegion.html('');
                    }
                });
            });
        }
    }
};


/* Set up Canvas experiements
   ========================================================================== */
TheLab.canvas = {
    canvas1: document.getElementById("canvas1"),
    init: function() {
        var _this = this;

        if (_this.canvas1 && _this.canvas1.getContext) {
            // TODO: change the width and height of the canvas
            _this.canvas1.width=150;
            _this.canvas1.height=150;

            // TODO: fill the canvas with a light blue bg
            var ctx = _this.canvas1.getContext("2d");
            if (ctx) {
                ctx.fillStyle = "lightBlue";
                ctx.strokeStyle = "blue";
                ctx.lineWidth = 5;

                ctx.fillRect(0,0,ctx.canvas.width,ctx.canvas.height);
                ctx.strokeRect(0,0,ctx.canvas.width,ctx.canvas.height);
            }
        }
    }
};



/* Initialize/Fire
   ========================================================================== */
TheLab.startup = {
    init : function () {
        //console.log("Initial load: scripting initializing");
        picturefill();

        TheLab.links.init();
        TheLab.responsiveTables.init();
        TheLab.forms.init();
        TheLab.ghostTitle.init();
        TheLab.toggletips.init();
        TheLab.canvas.init();
    },
    "page--home" : function() {
        //console.log("Homepage scripts");
    },
    finalize : function() {
        //console.log("Initial load: scripting finalized");
    }
};

TheLab.fire = function() {
    /* Fire based on document context 
    ========================================================================== */

    var namespace  = TheLab.startup, context = document.body.id;
    if (typeof namespace.init === 'function') {
        namespace.init();
    }
    if (namespace && namespace[ context ] && (typeof namespace[ context ] === 'function')) {
        namespace[ context ]();
    }
    if (typeof namespace.finalize === 'function') {
        namespace.finalize();
    }

    TheLab.isPageloaded = true;
};

jQuery(document).ready(function() {

    //console.log("Matchmedia? " + Modernizr.matchmedia);
    if (Modernizr.matchmedia) {
        //console.log("Nice browser!");

        jQuery.getScript('/ui/js/standalone/enquire.min.js').done(function() {
            //console.log("Enquire.js loaded!");
            jQuery.getScript('/ui/js/standalone/picturefill.min.js').done(function() {
                //console.log("Picturefill.js loaded!");
                TheLab.fire();
            });
        });
        
    } else {
        //console.log("Older browser!");
        jQuery.getScript('/ui/js/standalone/media.match.min.js').done(function() {
            //console.log("Media.match.js loaded!");
            jQuery.getScript('/ui/js/standalone/enquire.min.js').done(function() {
                //console.log("Enquire.js loaded!");
                jQuery.getScript('/ui/js/standalone/picturefill.min.js').done(function() {
                    //console.log("Picturefill.js loaded!");
                    TheLab.fire();
                });
            });
        });
    }
});