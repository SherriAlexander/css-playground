CSS Playground
====

Author: Sherri Alexander (sherri@sherri-alexander.com)
last updated August 21, 2019


Front-end project setup
-----------------------
The front-end templates are set up using Jekyll as our framework (mainly to take advantage of Jekyll's ability to create includes and page-level variables), and using Grunt as a task runner (which compiles our stylesheets, concatenates and minifies script files, and triggers Jekyll builds).  We are using the Sass pre-processing language to generate our CSS stylesheets.

The project contains two main directories:

* `_source` is the working directory where all the source files are kept (includes, Sass, etc).  This is where all front-end changes to HTML/JS/Sass should be made.  
* `_site` is the build folder, where all the final generated and processed HTML, JS, and CSS files are kept.  The final files are copied there automatically by Grunt and Jekyll.


References and documentation
----------------------------
* Getting started with Grunt: http://gruntjs.com/getting-started
* Getting started with Sass: http://sass-lang.com/guide
* Jekyll documentation: http://jekyllrb.com/docs/home/


Global dependencies
-------------------
Node.js, npm, and grunt are the global requirements for our development workflow.

To check if node.js is installed, run this command on the command line:

```
node -v
```    

If needed, you can download Node.js from the official website here:

https://docs.npmjs.com/getting-started/installing-node

To update NPM to the latest version (it's updated much more often than node.js), run this command:

```
npm install npm@latest -g
```

After installing Node.js, you'll need to install the grunt command line interface globally via npm (Note: if you are on a Unix system, you may need to use sudo, and if you are on Windows, you may need to run your command shell as Administrator):

```
npm install -g grunt-cli
```

If you are on Mac OS X and this is your first time setting up this dev environment, you may also need to install the following items:

* Ruby (via Homebrew, http://railsapps.github.io/installrubyonrails-mac.html) 
* SASS (sudo gem install sass)
* Jekyll (sudo gem install jekyll)


Setting up Grunt and its dependencies for the project
-----------------------------------------------------
Follow these steps to install the project's Grunt plugins:

1. Open Terminal or your command line of choice.
2. Change to the project's root directory.
3. Install project dependencies with `npm install` (or again, using `sudo` or the Administrator command shell if there are permissions issues).


Using Grunt and Jekyll
----------------------
To make any changes to the project's HTML, CSS, or Javascript, you will need to run Grunt.
Grunt is set up so that it's the primary task runner, and it will trigger the Jekyll builds as needed (no need to run any Jekyll commands).

To run Grunt:

1. Open Terminal or your command line of choice.
2. Change to the project's root directory (the directory containing `Gruntfile.js`).
3. Run Grunt with the `grunt` command (or `grunt init` if it's the first time setting up the project).

Grunt will do the rest!  

Grunt watches files in the `_source/` directory for changes, and will perform the following automatic actions:

* When an HTML file is changed, it will trigger Jekyll to compile the HTML file with all includes, and will copy the entire site into the `_site/` directory.
* When an image file is changed, it will trigger Jekyll to copy the entire site (including the new image) into the `_site/` directory.
* When a Sass file is changed, it will compile the CSS file and copy it to the `_site/ui/css/` directory.
* When a Javascript file is changed, it will check the JS for formatting errors, concatenate the JS files, minify the concatenated file, and copy it into the `_site/ui/js/` directory.
* I've included BrowserSync (http://www.browsersync.io/) which will automatically spin up a server, launch a browser window for the site, and watch the files and update in your browser window.

To stop the Grunt process, press `control-c` in your Terminal or command line window.


Includes
--------
Includes for the site are kept in the `_source/_includes/` directory.  We have created includes for many common structures for the site like the masthead, footer, etc.

Many includes make use of page-level variables via Jekyll.  These variables are located at the top of each HTML page in the root `_source/` directory.

For more information on Jekyll templates and variables, check out their documentation here:

* http://jekyllrb.com/docs/templates/
* http://jekyllrb.com/docs/variables/


Stylesheets
-----------
If you are unfamiliar with the Sass pre-processing language, check out the official Sass website for documentation and great information about why Sass is a great front-end development tool (and a handy time-saver):

http://sass-lang.com/

The Sass stylesheets are located in the `_source/ui/sass/` directory.  Here is a list of the Sass files and what they are for:

**styles.scss**  
The main concatenator for the stylesheets.  This master file pulls all the rest of the files into it.  Should rarely need to be changed.

**_vars.scss**  
Defines all the Sass variables used throughout the site.  Very useful for making cosmetic changes to the site (colors, borders, border radius, etc) if needed (or used as a jumping-off point for creating a new set of styles for another brand).  Should not need to be changed often.

**_mixins.scss**  
Useful "helpers" for the Sass files.  Should not need to be changed often for this site, but the font mixins might be adapted for different brand fonts on other sites.

**_normalize.scss**  
Normalizes styles as a baseline to build from.  Should not need to be changed.

**_webfonts.scss**  
Font definitions for the web fonts used on the site.  Should not need to be changed.

**_ui.scss**  
The majority of the site's styles are here.  This file is the most likely to be where changes are needed.


Scripts
-------
Scripts are located in the `_source/ui/js/` directory and subdirectories:

**a11y/***  
Accessibility-related scripts

**jquery/***  
jQuery dependencies

**plugins/***  
Javascript plugins used in the site

**standalone/***  
Javascripts which must be standalone files, they cannot be concatenated into the main scripts file

**scripts.js**  
This is where most of the site-specific functionality is kept, and where any changes should be made.
