module.exports = function(grunt) {

    "use strict";

    // -------------------------------------------------------------------------
    // #### Load task modules ####
    // This will load all of the modules that are defined in package.json >
    // devDependencies whose name begins with "grunt-".
    // https://github.com/tkellen/node-matchdep
    // -------------------------------------------------------------------------

    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);


    // -------------------------------------------------------------------------
    // #### Project configuration ####
    // -------------------------------------------------------------------------

    grunt.initConfig({


        // ---------------------------------------------------------------------
        // #### Variables ####
        // ---------------------------------------------------------------------

        devPath:      '_source/ui/',
        prodPath:     '_site/ui/',
        sassPath:     '<%= devPath %>sass/',
        cssPath:      '<%= devPath %>css/',
        cssPathProd:  '<%= prodPath %>css/',
        fontPath:     '<%= devPath %>css/fonts/',
        fontPathProd: '<%= prodPath %>css/fonts/',
        jsPathProd:   '<%= prodPath %>js/',
        jsPathDev:    '<%= devPath %>js/',
        imgPath:      '<%= devPath %>images/',
        imgPathProd:  '<%= prodPath %>images/',
        imgFileTypes: 'gif,jpg,jpeg,png',
        docFileTypes: '.html,.htm,.php',


        // ---------------------------------------------------------------------
        // #### Get data from package.json ####
        // Get data from the package.json file and assign it to a pkg variable.
        // ---------------------------------------------------------------------

        pkg: grunt.file.readJSON('package.json'),


        // ---------------------------------------------------------------------
        // #### Task configuration ####
        // ---------------------------------------------------------------------

        // Task: Sass compiling.
        // https://github.com/gruntjs/grunt-contrib-sass
        sass: {
            // Task-wide options.
            options: {sourcemap: 'none'},
            dist: {
                // Target-specific options.
                options: {
                    style: "expanded", // most easily human readable
                    // style: "compact", // rules on one line
                    // style: "compressed", // minified
                    noCache: true,
                    loadPath: '<%= sassPath %>'
                },
                expand: true,
                cwd: '<%= sassPath %>',
                src: ['*.scss'],
                dest: '<%= cssPath %>',
                ext: '-unprefixed.css'
            }
        },

        // Task: Copy the generated CSS over to _site
        // https://github.com/gruntjs/grunt-contrib-copy
        // http://thanpol.as/jekyll/jekyll-and-livereload-flow/
        copy: {
            css : {
                files: {
                  // Copy the SCSS-generated style files to
                  // the _site/ folder
                  '<%= cssPathProd %>styles.css': '<%= cssPath %>styles.css'
                }
            },
            js: {
                files: {
                    // Copy the script files to the _site/ folder
                    '<%= jsPathProd %>scripts-concat.min.js': '<%= jsPathDev %>scripts-concat.min.js'
                }
            },
            webfonts: {
                files: [
                    // Copy the webfont files to the _site/ folder
                    {expand:true, flatten:true, src:'<%= fontPath %>**', dest:'<%= fontPathProd %>', filter: 'isFile'}
                ]
            }
        },

        // Task: JavaScript hinting.
        // https://github.com/gruntjs/grunt-contrib-jshint
        jshint: {
            // Task-wide options.
            options: {},
            // Target: all.
            all: {
                // Target-specific options.
                options: {
                    evil: true,
                    loopfunc: true
                },
                // Source file(s), in the order we want to hint them.
                src: [
                    'Gruntfile.js',
                    '<%= jsPathDev %>*.js',
                    '!<%= jsPathDev %>*.min.js', // ...but not minified files.
                    '!<%= jsPathDev %>scripts-concat.js' // ...but not the concatenated file.
                ]
            }
        },

        // Task: JavaScript file concatenation.
        // https://github.com/gruntjs/grunt-contrib-concat
        concat: {
            // Task-wide options.
            options: {},
            // Target: all.
            all: {
                // Target-specific options.
                options: {
                    // Separate each concatenated script with a semicolon.
                    separator: ';'
                },
                // Source file(s), in the order we want to concatenate them.
                src: [
                    '<%= jsPathDev %>plugins/consolelog.js', // 1st to prevent console errors
                    '<%= jsPathDev %>a11y/*.js', // All accessibility scripts.
                    // Plugins
                    '<%= jsPathDev %>plugins/parsley.js',
                    // And last, our custom scripts
                    '<%= jsPathDev %>scripts.js'
                ],
                // Destination file.
                dest: '<%= jsPathDev %>scripts-concat.js',
                // Warn if a given file is missing or invalid.
                nonull: true
            }
        },

        // Task: JavaScript minification.
        // https://github.com/gruntjs/grunt-contrib-uglify
        uglify: {
            // Task-wide options.
            options: {
                compress: {
                    booleans: false
                    //    drop_console:true
                }
            },
            // Target: all.
            all: {
                // Target-specific options.
                options: {
                    // Report the original vs. minified file-size.
                    report: 'min'
                },
                // Source file(s), in the order we want to minify them.
                files: {
                    '<%= jsPathDev %>scripts-concat.min.js' : '<%= jsPathDev %>scripts-concat.js'
                }
            }
        },

        // Task: image minification.
        // https://github.com/JamieMason/grunt-imageoptim
        imageoptim: {
            all: {
                options: {
                    jpegMini: false,
                    imageAlpha: true,
                    quitAfter: true
                },
                src: ['<%= imgPathProd %>']
            }
        },


        // Task: Call Jekyll when non-CSS and non-JS files change.
        // http://thanpol.as/jekyll/jekyll-and-livereload-flow/
        shell: {
            jekyll: {
                command: 'rm -rf _site/*; jekyll build',
                stdout: true
            }
        },

        // Task: Run post-processing (currently only autoprefixer) on CSS.
        // https://github.com/postcss/postcss
        postcss: {
            options: {
                map: true,
                processors: [
                    require('autoprefixer')({
                        browsers: ['last 2 versions', 'ios 8', 'ie 9', 'ie 10', 'ie 11']
                    })
                ]
            },
            dist: {
                src: '<%= cssPath %>styles-unprefixed.css',
                dest: '<%= cssPath %>styles.css'
            }
        },


        // Task: run Browser Sync for easy cross-browser testing
        // http://www.browsersync.io/docs/grunt/
        browserSync: {
            dev: {
                options: {
                    server: {
                        baseDir: "./_site",
                        index: "index.html"
                    },
                    xip: true,
                    browser: "Firefox",
                    open: "external",
                    background: true
                }
            }
        },

        // Task: bsReload for BrowserSync reloading
        // https://github.com/BrowserSync/recipes/tree/master/recipes/grunt.sass.autoprefixer

        bsReload: {
            css: {
                reload: "<%= cssPathProd %>styles.css"
            },
            js: {
                reload: "<%= jsPathProd %>scripts-concat.min.js"
            },
            all: {
                reload: true
            }
        },


        // Task: aside from creating notifications when something fails (which
        // is this task's permanent/default behavior), also create the following
        // notifications for these custom, non-failure events. The targets below
        // are utilized by requesting them via targets found in the watch task.
        // https://github.com/dylang/grunt-notify
        notify: {
            // Target: Sass.
            sass: {
                options: {
                    title: 'CSS',
                    message: 'Compiled successfully.'
                }
            },
            // Target: JavaScript.
            js: {
                options: {
                    title: 'JavaScript',
                    message: 'Hinted, concatenated, and minified successfully.'
                }
            },
            // Target: Jekyll.
            shell: {
                options: {
                    title: 'Jekyll',
                    message: 'Jekyll site built successfully.'
                }
            },
            // Target: ready for production.
            imageoptim: {
                options: {
                    title: 'Crushing Images',
                    message: 'Images crushed successfully.'
                }
            }
        },

        // Task: when something changes, run specific task(s).
        // https://github.com/gruntjs/grunt-contrib-watch
        watch: {
            // Task-wide options.
            options: {
                spawn:false // for Browsersync to work correctly
            },
            // Target: Sass.
            sass: {
                // Watch all .scss files inside of sassPath.
                files: '<%= sassPath %>**/*.scss',
                // Task(s) to run.
                tasks: [
                    'sass',
                    'postcss:dist',
                    'copy:css',
                    'bsReload:css',
                    'notify:sass'
                ]
            },
            // Target: Jekyll sources.
            jekyllSources: {
                files: [
                    '_source/**/*.html',
                    '_source/**/**/*.html',
                    '_source/**/*.htm',
                    '_source/**/**/*.htm',
                    '_source/**/*.inc',
                    '_source/**/*.xml',
                    '_source/**/*.rss',
                    '_source/**/*.appcache',
                    '*.yml',
                    '<%= imgPath %>**/*',
                    '<%= fontPath %>**/*'
                ],
                tasks: [
                    'shell:jekyll',
                    'bsReload:all',
                    'notify:shell'
                ]
            },
            // Target: JavaScript.
            js: {
                // Watch all JavaScript files that are direct children of jsPath.
                files: [
                    '<%= jsPathDev %>/**/*.js',
                    '!<%= jsPathDev %>/**/*.min.js' // ...but not minified files.
                ],
                // Task(s) to run.
                tasks: [
                    'jshint:all',
                    'concat:all',
                    'uglify:all',
                    'copy:js',
                    'bsReload:js',
                    'notify:js'
                ]
            }
        }

    });


    // -------------------------------------------------------------------------
    // #### Define task aliases that run multiple tasks ####
    // On the command-line, type "grunt [alias]" without the square brackets or
    // double-quotes, and press return. The format is as follows:
    // grunt.registerTask('alias', [
    //     'task1',
    //     'task2:targetName',
    //     'task3'
    // ]);
    // -------------------------------------------------------------------------

    // #### Task alias: "default" ####
    // Task(s) to run when typing only "grunt" in the console.
    grunt.registerTask('default', [
        'browserSync',
        'watch'
    ]);

    // #### Task alias: "initSite" ####
    // Task(s) to run when initializing the site.
    grunt.registerTask('initSite', [
        'sass',
        'postcss:dist',
        'notify:sass',
        'jshint:all',
        'concat:all',
        'uglify:all',
        'notify:js',
        'shell:jekyll',
        'notify:shell'
    ]);

    // #### Task alias: "crushImages" ####
    // Tasks(s) to run when you want to crush PNGs using imageOptim
    grunt.registerTask('crushImages', [
        'imageoptim:all',
        'notify:imageoptim'
    ]);

    // #### Task alias: "unusedImages" ####
    // Find unused images
    grunt.registerTask('unusedImages', function(){
        var assets = [],
            links = [];

        // Get list of images
        grunt.file.expand({
            filter: 'isFile',
            cwd: '<%= imgPathProd %>'
        }, ['**/*']).forEach(function(file){
            assets.push(file);
        });

        // Find images in content
        grunt.file.expand({
            filter: 'isFile',
        }, ['<%= jsPathProd %>**/*.js', '<%= cssPathProd %>**/*.css']).forEach(function(file){ // Change this to narrow down the search
            var content = grunt.file.read(file);
            assets.forEach(function(asset){
                if(content.search(asset) !== -1){
                    links.push(asset);
                }
            });
        });

        // Output unused images
        var unused = grunt.util._.difference(assets, links);
        console.log('Found '+ unused.length +' unused images:');
        unused.forEach(function(el){
            console.log(el);
        });
    });

};